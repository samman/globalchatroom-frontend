import React, { useContext, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import axios from "./axios";
import { AuthContext } from "./AuthContext";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Samman Adhikari
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const classes = useStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [shouldShowUsernameRequired, setShouldShowUsernameRequired] =
    useState(false);
  const [shouldShowPasswordRequired, setShouldShowPasswordRequired] =
    useState(false);
  const [shouldRememberUser, setShouldRememberUser] = useState(false);
  const [serverErrorMsg, setServerErrorMsg] = useState("");
  const { setToken } = useContext(AuthContext);

  const handleSignin = async (e) => {
    if (!username) {
      setShouldShowUsernameRequired(true);
    } else {
      setShouldShowUsernameRequired(false);
    }
    if (!password) {
      setShouldShowPasswordRequired(true);
    } else {
      setShouldShowPasswordRequired(false);
    }

    if (username && password) {
      try {
        const res = await axios.post("/auth/signin", { username, password });
        if (shouldRememberUser) localStorage.setItem("token", res.data.token);
        setToken(res.data.token);
      } catch (err) {
        setServerErrorMsg(err.response.data.message);
      }
    }
  };

  const handleSignup = async (e) => {
    if (!username) {
      setShouldShowUsernameRequired(true);
    } else {
      setShouldShowUsernameRequired(false);
    }
    if (!password) {
      setShouldShowPasswordRequired(true);
    } else {
      setShouldShowPasswordRequired(false);
    }
    if (username && password) {
      try {
        const res = await axios.post("/auth/signup", { username, password });
        if (shouldRememberUser) localStorage.setItem("token", res.data.token);
        setToken(res.data.token);
      } catch (err) {
        setServerErrorMsg(err.response.data.message);
      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Snackbar
        open={!!serverErrorMsg}
        autoHideDuration={6000}
        onClose={() => setServerErrorMsg("")}
      >
        <Alert severity="error" onClose={() => setServerErrorMsg("")}>
          {serverErrorMsg}
        </Alert>
      </Snackbar>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            value={username}
            onInput={(e) => setUsername(e.target.value)}
            error={shouldShowUsernameRequired}
            helperText={shouldShowUsernameRequired && "Username is required."}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onInput={(e) => setPassword(e.target.value)}
            error={shouldShowPasswordRequired}
            helperText={shouldShowPasswordRequired && "Password is required."}
          />
          <FormControlLabel
            control={
              <Checkbox
                value="remember"
                color="primary"
                checked={shouldRememberUser}
                onChange={() => setShouldRememberUser((prev) => !prev)}
              />
            }
            label="Remember me"
            value={true}
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            id="signin"
            className={classes.submit}
            onClick={handleSignin}
          >
            Sign In
          </Button>
          <Button
            fullWidth
            variant="contained"
            color="secondary"
            id="signup"
            className={classes.submit}
            onClick={handleSignup}
          >
            Sign Up
          </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
