import { Redirect, Route } from "react-router-dom";

const RouteGuard = ({
  component: Component,
  auth,
  redirectTo = "/signin",
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        auth ? <Component {...props} /> : <Redirect to={redirectTo} />
      }
    />
  );
};

export default RouteGuard;
