import React, { useContext, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Pusher from "pusher-js";
import axios from "./axios";
import Sidebar from "./Sidebar";
import Chat from "./Chat";
import { AuthContext } from "./AuthContext";
import SignoutConfirmationDialog from "./LogoutDialog";

const Home = () => {
  const [messages, setMessages] = useState([]);
  const [shouldShowSignoutConfirmation, setShouldShowSignoutConfirmation] = useState(false);
  const { token, setToken } = useContext(AuthContext);
  const history = useHistory();

  useEffect(() => {
    axios
      .get("/messages/sync", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setMessages(res.data);
      });
  }, []);

  useEffect(() => {
    const pusher = new Pusher("8f8de499e6f051bc7589", {
      cluster: "ap2",
    });

    const channel = pusher.subscribe("messages");
    channel.bind("inserted", function (data) {
      setMessages((prev) => [...prev, data]);
    });

    return () => {
      channel.unbind_all();
      channel.unsubscribe();
    };
  }, [messages]);

  const handleSignout = ()=> {
    setToken("");
    localStorage.removeItem("token");
    setShouldShowSignoutConfirmation(false);
    history.push("/signin")
  }

  return (
    <>
      <SignoutConfirmationDialog onConfirm={handleSignout} open={shouldShowSignoutConfirmation} onClose={()=>setShouldShowSignoutConfirmation(false)}/>
      {/* Sidebar component */}
      <Sidebar />
      {/* Chat component */}
      <Chat messages={messages} onSignoutClick={()=>setShouldShowSignoutConfirmation(true)} />
    </>
  );
};

export default Home;
