import React, { createContext, useState, useEffect } from "react";
import axios from "./axios";

export const AuthContext = createContext({
  token: "",
});

export const AuthContextProvider = (props) => {
  const [token, setToken] = useState("");
  const [loggedInUser, setLoggedInUser] = useState();

  useEffect(() => {
    const storedToken = localStorage.getItem("token");
    setToken(storedToken);
  }, []);

  useEffect(() => {
    if (token) {
      (async () => {
        try {
          const res = await axios.get("/auth/profile", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          setLoggedInUser(res.data.user);
        } catch (err) {
          console.log(err);
        }
      })();
    }
  }, [token]);

  return (
    <AuthContext.Provider
      value={{ token, setToken, loggedInUser, setLoggedInUser }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};
