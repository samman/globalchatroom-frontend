import { useContext } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import "./App.css";
import Home from "./Home";
import SignIn from "./SignIn";
import { AuthContext } from "./AuthContext";
import RouteGuard from "./RouteGuard";

function App() {
  const { token } = useContext(AuthContext);

  return (
    <div className="app">
      <div className="app__body">
        <BrowserRouter>
          <Switch>
            <RouteGuard path="/" exact component={Home} auth={!!token} />
            <RouteGuard
              path="/signin"
              exact
              component={SignIn}
              redirectTo={"/"}
              auth={!token}
            />
          </Switch>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
