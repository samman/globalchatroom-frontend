import React, { useState, useContext } from "react";
import { IconButton, Avatar } from "@material-ui/core";
import {
  AttachFile,
  InsertEmoticon,
  MoreVert,
  SearchOutlined,
  Mic,
  ExitToApp,
} from "@material-ui/icons";
import "./Chat.css";
import axios from "./axios";
import { AuthContext } from "./AuthContext";

function Chat(props) {
  const [message, setMessage] = useState("");
  const { loggedInUser, token, setToken } = useContext(AuthContext);

  const sendMessage = (e) => {
    e.preventDefault();
    axios.post(
      "/messages/new",
      {
        message,
        timestamp: new Date().toUTCString(),
      },
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    );
    setMessage("");
  };

  return (
    <div className="chat">
      <div className="chat__header">
        <Avatar />

        <div className="chat__headerInfo">
          <h3>{loggedInUser?.username}</h3>
          {/* <p>Last seen at ...</p> */}
        </div>
        <div className="chat__headerRight">
          <IconButton>
            <SearchOutlined />
          </IconButton>
          <IconButton>
            <AttachFile />
          </IconButton>
          <IconButton>
            <MoreVert />
          </IconButton>
          <IconButton onClick={props.onSignoutClick}>
            <ExitToApp />
          </IconButton>
        </div>
      </div>

      <div className="chat__body">
        {props.messages.map((message) => (
          <p
            className={`chat__message ${
              loggedInUser?._id !== message.sender_id && "chat__receiver"
            }`}
            key={message._id}
          >
            <span className="chat__name">{message.sender_username}</span>
            {message.message}
            <span className="chat__timestamp">{message.timestamp}</span>
          </p>
        ))}
      </div>

      <div className="chat__footer">
        <InsertEmoticon />
        <form>
          <input
            placeholder="Type a message"
            type="text"
            value={message}
            onInput={(e) => setMessage(e.target.value)}
          />
          <button type="submit" onClick={sendMessage}>
            Send a message
          </button>
        </form>
        <Mic />
      </div>
    </div>
  );
}

export default Chat;
