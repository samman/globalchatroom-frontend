import React from 'react'
import  { Avatar } from "@material-ui/core";

import "./SidebarChat.css"

function SidebarChat() {
    return (
        <div className="sidebarChat">
            <Avatar />
            <div className="sidebarChat__info">
                <h2>Global Public Room</h2>
                <p>Sorry! this is the only chat room for now</p>
            </div>
        </div>
    )
}

export default SidebarChat;